# Make sure to run l2tp script first!!!

apt-get -y install pptpd
 
cat >> /etc/pptpd.conf <<EOF
localip 192.168.0.1
remoteip 192.168.1.2-255
EOF
 
cat >> /etc/ppp/pptpd-options <<EOF
ms-dns 8.8.8.8
ms-dns 8.8.4.4
EOF
 
cat >> /etc/ppp/chap-secrets <<EOF
halo pptpd MagicalMushrooms007 *
EOF
 
cat >> /etc/sysctl.conf <<EOF
net.ipv6.conf.all.forwarding=1
EOF
 
sysctl -p
 
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
 
/etc/init.d/pptpd restart
reboot

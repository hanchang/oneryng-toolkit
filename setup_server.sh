# Security Decisions:
# Allow root login since OneRyng app needs to be root to add user to chap-secrets
# Change SSH port to random
# Change root password to 24 character randomly generated
# Install denyhosts to prevent brute force attacks 

HOSTNAME=`hostname`

# Get server data
IP=`/sbin/ifconfig eth0 | grep "inet addr" | awk -F: '{print $2}' | awk '{print $1}'`
echo $IP
echo "IP Address: $IP" >> $HOSTNAME.log

PORT=`perl -e 'print int(rand(65000-2000)) + 2000'`
echo $PORT
echo "SSH Port: $PORT" >> $HOSTNAME.log

PASSWORD=`< /dev/urandom tr -dc _A-Z-a-z-1-9 | head -c24`
echo $PASSWORD
echo "SSH Password: $PASSWORD" >> $HOSTNAME.log

echo "root:$PASSWORD" | sudo chpasswd -m

# Change SSH to random port
sed "s/Port 22/Port $PORT/i" -i /etc/ssh/sshd_config

# Prevent IP Spoofing
cat > /etc/host.conf <<EOF
order bind,hosts
nospoof on
multi on
EOF

# Add DenyHosts to prevent SSH brute forcing
apt-get -y install denyhosts

# Hope nothing went wrong!
service ssh restart

echo "Don't forget to enter the data above into Google Docs!\n"
